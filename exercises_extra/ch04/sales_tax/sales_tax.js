"use strict"

const MINSUBTOTAL = 0;
const MAXSUBTOTAL = 10000;
const MINTAXRATE = 0;
const MAXTAXRATE = 12;
const MINPPG = 1.00;
const MAXPPG = 5.00;


var $ = function (id) {
    return document.getElementById(id);
};

var processEntries = function() {
        var subtotal = parseFloat($("subtotal").value);
        var tax_rate = parseFloat($("tax_rate").value);
        var validInput = true;

        if (isNaN(subtotal)){
          $("subtotal").nextElementSibling.firstChild.nodeValue = "Numeric input required";
          validInput = false;
        }
        else if ((subtotal < MINSUBTOTAL) || (subtotal > MAXSUBTOTAL)){
          $("subtotal").nextElementSibling.firstChild.nodeValue = "Subtotal must be > 0 and < 10000";
          validInput = false;
        }
        if (isNaN(tax_rate)){
          $("tax_rate").nextElementSibling.firstChild.nodeValue = "Numeric input required";
          validInput = false;
        }
        else if ((tax_rate < MINTAXRATE) || (tax_rate > MAXTAXRATE)){
          $("gallons").nextElementSibling.firstChild.nodeValue = "Tax Rate must be > 0 and < 12";
          validInput = false;
        }

        if (validInput){
          calculateSalesTax(subtotal, tax_rate);
          calculateTotal(subtotal, tax_rate, total, sales_tax);
        }
};


var calculateSalesTax = function(s, t){
  var sales_tax = ((t/100)*s).toFixed(2);
  $("sales_tax").value = sales_tax;
};

var calculateTotal = function(subtotal, sales_tax){
  var total = (subtotal+sales_tax).toFixed(2);
  $("total").value = total;
};

var resetTheForm = function() {
  $("subtotal").value = "";
  $("tax_rate").value = "";
  $("sales_tax").value = "";
  $("total").value = "";
  $("subtotal").nextElementSibling.firstChild.nodeValue = "*";
  $("tax_rate").nextElementSibling.firstChild.nodeValue = "*";
  $("subtotal").focus();

}

window.onload=function(){
  $("calculate").onclick = processEntries;
  $("clear").onclick = resetTheForm;
  $("subtotal").focus();
};
