$(document).ready(function() {

	var isValid = true;
	var regexpattern = "";

	$("submit").onclick		=	validateForm;

	var validateForm = function (){

		$(#arrival_date).focus();

		function validateNights(a)
		{
			if (nights.trim() == NaN)
			{
				$("nights_error").firstChild.nodeValue =
								"Enter numeric value";
				isValid = false;
			}
			else
			{
				$("nights_error").firstChild.nodeValue = "";
				string += "Arival date:  " + a + "\n";
			}
		}

		function validateName(f)
	  {
	  	// Validate the First Name
	  	if (f.trim() == "")
	  	{
	  		$("fname_error").firstChild.nodeValue =
	  						"First Name Required.";
	  		isValid = false;
	  	}
	  	else
	  	{
	  		regexpattern = /^([A-Za-z]){1,50}$/;

	  		if (!f.match(regexpattern))
	  		{
	  			$("fname_error").firstChild.nodeValue =
	  						"First Name Must Be Alpha Only.";
	  			isValid = false;
	  		}
	  		else
	  		{
	  			$("fname_error").firstChild.nodeValue = "";
	  			string += "First Name:  " + f + "\n";
	  		}
	  	}
	  }



		function validatePhone(c)
	  {
	  	// Validate the Phone Number
	  	if (c.trim() == "")
	  	{
	  		$("phone_error").firstChild.nodeValue =
	  						"Phone Number Required.";
	  		isValid = false;
	  	}
	  	else
	  	{
	  		regexpattern = /^\d{3}-\d{3}-\d{4}$/;

	  		if (!c.match(regexpattern))
	  		{
	  			isValid = false;
	  			$("cellphone_error").firstChild.nodeValue =
	  						"# must be in nnn-nnn-nnnn format.";
	  		}
	  		else
	  		{
	  			$("cellphone_error").firstChild.nodeValue = "";
	  			string += "Phone Number:  " + c + "\n";
	  		}
	  	}
	  }

		function validateEmail(e)
	  {
	  	// Validate the Phone Number
	  	if (e.trim() == "")
	  	{
	  		$("email_error").firstChild.nodeValue =
	  						"Phone Number Required.";
	  		isValid = false;
	  	}
	  	else
	  	{
	  		regexpattern = /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b/;

	  		if (!e.match(regexpattern))
	  		{
	  			isValid = false;
	  			$("email_error").firstChild.nodeValue =
	  						"# must be a valid email address";
	  		}
	  		else
	  		{
	  			$("email_error").firstChild.nodeValue = "";
	  			string += "Email:  " + e + "\n";
	  		}
	  	}
	  }


		validateNights(nights);
		validateName(name);
		validateEmail(email);
		validatePhone(phone);
	};


}); // end ready
