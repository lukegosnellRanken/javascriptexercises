"use strict"

const MINCHANGE = 0;
const MAXCHANGE = 99;
var pennies = 0;
var nickels = 0;
var dimes = 0;
var quarters = 0;
var validInput = true;

var $ = function(id) {
    return document.getElementById(id);
};

var processEntries = function() {
        var cents = parseFloat($("cents").value);
        validInput = true;

        if (isNaN(cents)){
          $("cents").nextElementSibling.firstChild.nodeValue = "Numeric input required";
          validInput = false;
        }
        else if ((cents < MINCHANGE) || (cents > MAXCHANGE)){
          $("cents").nextElementSibling.firstChild.nodeValue = "Must be between 0 and 99";
          validInput = false;
        }

        if (validInput){
          makeChange(cents);
        }

};

var makeChange = function(cents){
  var changeLeft = 0;

  quarters = (cents/25).toFixed(0);
  changeLeft = cents % 25;

  dimes = (cents/10).toFixed(0);
  changeLeft = cents % 10;

  nickels = (cents/5).toFixed(0);
  pennies = changeLeft % 5;

  $("quarters").value = quarters;
  $("dimes").value = dimes;
  $("nickels").value = nickels;
  $("pennies").value = pennies;

};

window.onload=function(){
  $("calculate").onclick = processEntries;
  $("cents").focus();
};
