"use strict";

const MINVALUE = 0;
const BRACKETONE = 9275;
const BRACKETTWO = 37650;
var tax = "";


var $ = function (id) {
    return document.getElementById(id);
};

var processEntries = function() {
        var income = parseFloat($("income").value);
        var validInput = true;

        if (isNaN(income)){
          $("income").value = "Numeric input required";
          validInput = false;
        }
        else if (income < MINVALUE){
          $("income").value = "income must be > 0 and less than 37650";
          validInput = false;
        }

        if (validInput){
          calculateTax(income);
        }
};

var calculateTax = function(income) {
  if ((income >= MINVALUE) && (income <= BRACKETONE)) {
    tax = ((income % 10).toFixed(0));
    $("tax").value = tax;
  }
  else if ((income >= BRACKETONE) &&  (income <= BRACKETTWO)) {
    tax = ((income % 15).toFixed(0));
    $("tax").value = tax;
  }
}

window.onload = function () {
    $("calculate").onclick = processEntries;
    $("income").focus();
};
